provider "aws" {
  region = "us-east-2"
}

# Define the PVC
resource "aws_vpc" "gorilla-test-vpc" {
  cidr_block = "10.0.0.0/16"
}

# Define the subnets
resource "aws_subnet" "gorilla_subnetA" {
  vpc_id            = aws_vpc.gorilla-test-vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-2a"
}

resource "aws_subnet" "gorilla_subnetB" {
  vpc_id            = aws_vpc.gorilla-test-vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-2b"
}

resource "aws_subnet" "gorilla_public_subnet" {
  vpc_id                  = aws_vpc.gorilla-test-vpc.id
  cidr_block              = "10.0.5.0/24"
  availability_zone       = "us-east-2c"
  map_public_ip_on_launch = true
}

# Define the NAT gateway
resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat_gw.id
  subnet_id     = aws_subnet.gorilla_public_subnet.id
}

resource "aws_route_table" "private_rt" {
  vpc_id = aws_vpc.gorilla-test-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw.id
  }
}

# Associate the private route table with the private subnet
resource "aws_route_table_association" "subnetA_association" {
  subnet_id      = aws_subnet.gorilla_subnetA.id
  route_table_id = aws_route_table.private_rt.id
}

# Associate the private route table with the private subnet
resource "aws_route_table_association" "subnetB_association" {
  subnet_id      = aws_subnet.gorilla_subnetB.id
  route_table_id = aws_route_table.private_rt.id
}

# Define the Elastic IP for the NAT gateway
resource "aws_eip" "nat_gw" {
  vpc = true
}

# Define an internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.gorilla-test-vpc.id
}

# Define route table for the public subnet
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.gorilla-test-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table_association" "public_route_table_association" {
  subnet_id      = aws_subnet.gorilla_public_subnet.id
  route_table_id = aws_route_table.public_rt.id
}