# Define the route for the ALB to to the IGW
resource "aws_route" "alb_igw_route" {
  route_table_id         = aws_route_table.public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

# Define the security group for the Application Load Balancer
# It allows traffic from everywhere to port 80 and 443
resource "aws_security_group" "alb_sg" {
  name_prefix = "alb_sg"
  vpc_id      = aws_vpc.gorilla-test-vpc.id

  ingress {
    from_port   = 0
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Define the actual ALB
resource "aws_lb" "gorilla_alb" {
  name               = "gorilla-alb"
  load_balancer_type = "application"
  internal           = false
  subnets            = ["${aws_subnet.gorilla_public_subnet.id}", "${aws_subnet.gorilla_subnetA.id}", "${aws_subnet.gorilla_subnetB.id}"]
  security_groups    = ["${aws_security_group.alb_sg.id}"]
}

# Define the HTTP listener for the ALB
# This will redirect traffic from port 80 to 443 (HTTPS)
resource "aws_lb_listener" "gorilla_https_redirect_listener" {
  load_balancer_arn = aws_lb.gorilla_alb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    target_group_arn = aws_lb_target_group.gorilla_tg.arn
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

# Define the HTTP listener for the ALB
# This will do the actual forwarding to the ECS
resource "aws_lb_listener" "gorilla_forward_listener" {
  load_balancer_arn = aws_lb.gorilla_alb.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:us-east-2:877064830116:certificate/25f1e03f-025f-44f1-a4c6-52b5794db121"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.gorilla_tg.arn
  }
}

# Define the target group for the ALB 
resource "aws_lb_target_group" "gorilla_tg" {
  port        = 3000
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.gorilla-test-vpc.id

  health_check {
    path     = "/login"
    interval = 30
    timeout  = 15
    port     = 3000
    protocol = "HTTP"
    matcher  = "200-299"
  }
}