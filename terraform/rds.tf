# Define the security group for RDS
resource "aws_security_group" "rds_sg" {
  name_prefix = "rds_sg"
  vpc_id      = aws_vpc.gorilla-test-vpc.id

  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.fargate_sg.id]
  }
}

# Define an RDS instance with MariaDB
resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "rds_subnet_group"
  subnet_ids = ["${aws_subnet.gorilla_subnetA.id}", "${aws_subnet.gorilla_subnetB.id}"]
}

resource "aws_db_instance" "gorilla_rds" {
  identifier             = "gorilla-rds"
  engine                 = "mariadb"
  engine_version         = "10.5"
  instance_class         = "db.t3.micro"
  allocated_storage      = 10
  db_name                = ""
  username               = ""
  password               = ""
  publicly_accessible    = false
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.rds_sg.id]

  db_subnet_group_name = aws_db_subnet_group.rds_subnet_group.name
}