# Define the security group for fargate. It will only accept traffic on port 3000 from any port that comes from the ALB
resource "aws_security_group" "fargate_sg" {
  name_prefix = "fargate_sg"
  vpc_id      = aws_vpc.gorilla-test-vpc.id

  ingress {
    from_port   = 0
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Define the ECS cluster
resource "aws_ecs_cluster" "gorilla-test-cluster" {
  name = "gorilla-test-cluster"
}

# Define the task definition for the ECS load
resource "aws_ecs_task_definition" "gorilla-test-task-definition" {
  family                   = "gorilla-test-task-definition"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 512
  memory                   = 1024
  container_definitions = jsonencode([{
    name   = "timeoff-management"
    image  = "registry.gitlab.com/andbuitra/gorilla-test:latest"
    cpu    = 512
    memory = 1024
    portMappings = [
      {
        containerPort = 3000,
        protocol      = "tcp"
      }
    ]
  }])
}

# Define the actual ECS service
resource "aws_ecs_service" "gorilla-test" {
  name            = "gorilla-test"
  cluster         = aws_ecs_cluster.gorilla-test-cluster.arn
  task_definition = aws_ecs_task_definition.gorilla-test-task-definition.arn
  desired_count   = 3
  launch_type     = "FARGATE"

  deployment_controller {
    type = "ECS"
  }

  network_configuration {
    assign_public_ip = false
    subnets          = ["${aws_subnet.gorilla_subnetA.id}", "${aws_subnet.gorilla_subnetB.id}"]
    security_groups  = ["${aws_security_group.fargate_sg.id}"]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.gorilla_tg.arn
    container_name   = "timeoff-management"
    container_port   = 3000
  }

}