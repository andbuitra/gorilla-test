* This Terraform is non-functional. This is on purpose. In order to not overcomplicate this POC, sensitive information has been removed from the main Terraform file (mainly the credentials to the RDS database).

* Terraform's tfstate is *not* present on the repository. This is also on purpose to avoid revealing sensitive data.

* This project's infrastructure is up and running on AWS. GitOps is out of the scope of the POC.