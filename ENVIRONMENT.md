Because of simplicity a lot of credentials are not handled like they would in the real world. However, security is kept in mind and that's why there's some friction on deployment.

When running the pipeline the following variables have to be passed manually:

$AWS_ACCESS_KEY_ID
$AWS_SECRET_ACCESS_KEY

$DB_USER
$DB_PASSWORD
$DB_NAME
$DB_HOST

$CI_REGISTRY_USER
$CI_REGISTRY_PASSWORD