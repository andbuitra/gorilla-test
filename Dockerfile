FROM node:14
WORKDIR /usr/src/app

# Copy the contents of the app and use "npm ci" to expedite the process
COPY timeoff-management-application-master .
RUN npm install && npm i mysql

# Run the app as non-root for security
RUN addgroup --system --gid 1337 app && adduser --uid 1337 --system --group app && chown -R app.app * && apt update -y && apt install sqlite3 -y
USER app

EXPOSE 3000
ENV NODE_ENV=production
CMD [ "npm", "start" ]
